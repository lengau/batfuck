#!/usr/bin/env python3

import sys

in_file = open(sys.argv[1])
out_file = open(sys.argv[2], 'w+')

bf_map = ' <>+-.,[]'

for line in in_file:
    command = line.split(' ', 1)[0].lower()
    command_num = command.count('na')
    out_file.write(bf_map[command_num])
    
