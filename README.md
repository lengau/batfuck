# batfuck

Batman-themed brainfuck preprocessor.

## Usage

```
python3 batfuck.py input_file.batfuck output_file.bf
```

Once you've converted the file to brainfuck, use any brainfuck compiler to convert to machine code

## Syntax

The syntax is very simple. The first word of each line (delimited by a space) must be a number of "na" digraphs. Each corresponds to a brainfuck command, as seen in the source code.

The program should terminate with the word "Batman!" but this is not enforced by the compiler.

## License
This is licenced under CC0, WTFPL, MIT, BSD, GPLv3, AGPLv3, and LGPLv3. You may use it under the terms of any of those licenses.

## Project status
I'm probably never gonna touch this again.
